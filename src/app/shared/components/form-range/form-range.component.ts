import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'shared-form-range',
  templateUrl: './form-range.component.html',
  styleUrls: ['./form-range.component.scss']
})
export class FormRangeComponent implements OnInit {
  form: FormGroup;

  @Input() size: string;
  @Output() filter: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
    this.form = new FormGroup({
      from: new FormControl('', []),
      to: new FormControl('', [])
    });
  }

  ngOnInit() {
  }

  filterData() {
    const data = this.form.value;
    data.from = parseInt(data.from, 10);
    data.to = parseInt(data.to, 10);
    this.filter.emit(data);
  }

}
