import { Component, OnInit, Input } from '@angular/core';
import { CarService } from '../../services';

@Component({
  selector: 'shared-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  @Input() product: any;

  constructor(
    private _car: CarService
  ) { }

  ngOnInit() {
  }

  addCar(product: any) {
    this._car.addProduct(product);
  }

}
