import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'shared-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.scss']
})
export class ListProductsComponent implements OnInit {

  products: any[];
  orderForm: FormGroup;

  productsOrder: any[];

  constructor() {
    this.orderForm = new FormGroup({
      order: new FormControl('', [])
    });

    (<FormControl>this.orderForm.get('order')).valueChanges
      .subscribe((value: string) => {
        this.orderData(value);
      });
  }

  ngOnInit() {
  }

  @Input('products')
  set setProducts(products: any[]) {
    this.products = products;
    this.orderData('');
  }

  orderData(typeFilter: string) {
    if (this.products) {
      this.productsOrder = [];
      this.productsOrder = this.products.slice(0);
      switch (typeFilter) {
        case '1':
          this.productsOrder.sort((a, b) => {
            return a.price - b.price;
          });
          break;
        case '2':
          this.productsOrder.sort((a, b) => {
            return b.price - a.price;
          });
          break;
        case '3':
          this.productsOrder.sort((a, b) => {
            return a.quantity - b.quantity;
          });
          break;
        case '4':
          this.productsOrder.sort((a, b) => {
            return b.quantity - a.quantity;
          });
          break;
        default:
          break;
      }
    }
  }

}
