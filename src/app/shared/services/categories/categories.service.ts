import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable()
export class CategoriesService {
  API: string = environment.API_URL;

  constructor(private _http: HttpClient) { }

  getAll() {
    const url = `${this.API}/categories.json`;
    return this._http.get(url)
      .pipe(map((response) => {
        return response['categories'];
      }));
  }
}
