import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class CarService {
  readonly KEY: string = 'Rappi-Car';

  private carModel: any;
  car: BehaviorSubject<any>;

  constructor() {
    this.carModel = { products: [] };
    this.car = new BehaviorSubject(this.carModel);

    this.load();
  }

  get() {
    let data = this.getData();
    if (!data) {
      this.clean();
      data = this.carModel;
    }
    return data;
  }
  set(car: any) {
    this.carModel = car;
    this.save();
  }

  getProduct(productIn) {
    let productExt = null;
    for (const product of this.carModel.products) {
      if (product.id === productIn.id) {
        productExt = product;
      }
    }
    return productExt;
  }
  addProduct(productIn) {
    const productExt = this.getProduct(productIn);
    if (productExt) {
      productExt.cant = productExt.cant + 1;
    } else {
      productIn['cant'] = 1;
      this.carModel.products.push(productIn);
    }
    this.save();
  }
  removeProduct(productIn) {
    const productsTmp = [];
    for (const product of this.carModel.products) {
      if (product.id !== productIn.id) {
        productsTmp.push(product);
      }
    }
    this.carModel.products = productsTmp;
    this.save();
  }

  save() {
    const data = JSON.stringify(this.carModel);
    localStorage.setItem(this.KEY, data);
    this.car.next(this.carModel);
  }
  clean() {
    this.carModel = { products: [] };
    this.save();
  }

  private getData() {
    const data = JSON.parse(localStorage.getItem(this.KEY));
    return data;
  }
  private load() {
    const data = this.getData();
    if (data) {
      this.carModel = data;
    }
    this.save();
  }

}
