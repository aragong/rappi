import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { CategoriesService, ProductsService, CarService } from './services';
import {
  FormRangeComponent,
  ListProductsComponent,
  MenuItemComponent,
  ProductComponent
} from './components';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([]),
    HttpClientModule,
    ReactiveFormsModule
  ],
  declarations: [
    FormRangeComponent,
    ListProductsComponent,
    MenuItemComponent,
    ProductComponent
  ],
  providers: [
    CarService,
    CategoriesService,
    ProductsService
  ],
  exports: [
    FormRangeComponent,
    ListProductsComponent,
    MenuItemComponent,
    ProductComponent
  ]
})
export class SharedModule { }
