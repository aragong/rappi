import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/shared/services';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  products: any[];
  categoryId: any;

  constructor(
    private _products: ProductsService,
    private _route: ActivatedRoute
  ) {
    this._route.params
      .subscribe((response) => {
        if (response['id']) {
          this.categoryId = parseInt(response['id'], 10);
          this.loadData();
        }
      });
  }

  ngOnInit() {
  }

  loadData() {
    this._products.getAll()
      .pipe(map((response: any[]) => {
        const data = [];
        for (const item of response) {
          if (item.sublevel_id === this.categoryId) {
            data.push(item);
          }
        }
        return data;
      }))
      .subscribe((response: any[]) => {
        this.products = response;
      });
  }

}
