import { Component, OnInit } from '@angular/core';
import { CarService } from 'src/app/shared/services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit {

  products: any[];

  constructor(
    private _car: CarService,
    private _router: Router
  ) {
    this.products = this._car.get().products;
  }

  ngOnInit() {
  }

  deleteProduct(product) {
    this._car.removeProduct(product);
    this.products = this._car.get().products;
  }

  pay() {
    this._car.clean();
    this.products = this._car.get().products;
    setTimeout(() => {
      this._router.navigate(['/']);
    }, 3000);
  }

}
