import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductsService } from 'src/app/shared/services';
import { ListProductsComponent } from 'src/app/shared/components';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  products: any[];
  productsFilter: any[];

  constructor(
    private _products: ProductsService
  ) {
    this._products.getAll()
      .subscribe((response: any[]) => {
        this.products = response;
        this.filter();
      });
  }

  ngOnInit() {
  }

  filter(filter?: any) {
    this.productsFilter = [];
    for (const product of this.products) {
      let add = true;
      if (filter) {
        if (filter.method === 'available') {
          if (filter.value && product.available !== filter.value) {
            add = false;
          }
        }
        if (filter.method === 'price') {
          if (filter.value['from'] && product.price < filter.value['from']) {
            add = false;
          }
          if (filter.value['to'] && product.price > filter.value['to']) {
            add = false;
          }
        }
        if (filter.method === 'quantity') {
          if (filter.value['from'] && product.quantity < filter.value['from']) {
            add = false;
          }
          if (filter.value['to'] && product.quantity > filter.value['to']) {
            add = false;
          }
        }
      }
      if (add) {
        this.productsFilter.push(product);
      }
    }
  }

}
