import { Routes , RouterModule} from '@angular/router';
import { PageComponent } from '../layouts/components';
import {
  LandingComponent, CategoryComponent, CarComponent
} from './components';

const ROUTES: Routes = [
  { path: '', component: PageComponent, children: [
    { path: '', component: LandingComponent },
    { path: 'category/:id/:name', component: CategoryComponent },
    { path: 'car', component: CarComponent },
    { path: '**', redirectTo: '', pathMatch: 'full' }
  ] }
];

export const PAGES_ROUTES = RouterModule.forChild(ROUTES);
