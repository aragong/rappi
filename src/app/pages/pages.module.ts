import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PAGES_ROUTES } from './pages.routes';
import { LayoutsModule } from '../layouts/layouts.module';
import { SharedModule } from '../shared/shared.module';
import {
  LandingComponent, CategoryComponent, CarComponent
} from './components';

@NgModule({
  imports: [
    CommonModule,
    LayoutsModule,
    SharedModule,
    PAGES_ROUTES
  ],
  declarations: [
    CarComponent,
    CategoryComponent,
    LandingComponent
  ]
})
export class PagesModule { }
