import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import {
  HeaderComponent,
  PageComponent,
  SidebarComponent
} from './components';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([]),
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    HeaderComponent,
    PageComponent,
    SidebarComponent
  ],
  exports: [
    HeaderComponent,
    PageComponent,
    SidebarComponent
  ]
})
export class LayoutsModule { }
