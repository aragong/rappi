import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'layout-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  formFilter: FormGroup;
  @Output() filter: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
    this.formFilter = new FormGroup({
      available: new FormControl(false, [])
    });
    (<FormControl>this.formFilter.get('available')).valueChanges
      .subscribe((value) => {
        this.filterData('available', value);
      });
  }

  ngOnInit() {
  }

  filterData(method: string, value: any) {
    this.filter.emit({
      method: method,
      value: value
    });
  }

}
