import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/shared/services/categories/categories.service';

@Component({
  selector: 'layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  items: any[];

  constructor(
    private _categories: CategoriesService
  ) {
    this._categories.getAll()
      .subscribe((response: any[]) => {
        this.items = response;
      });
  }

  ngOnInit() {
  }

}
